# README #

Transformers App

### What is this repository for? ###

This Transformers app is the part 2 of the take home assignment


### assumptions made upon special rules ###

- My understanding of the first special rule is that if Optimus Prime faces Soundwave, Optimus Prime automatically wins this battle, then the next battle continues. 
- As for the second special rule, my understanding is that if Optimus Prime faces Predaking, entire transformers that are in the game are destroyed, including the transformers that are not even in any battle? Basically, the game ends as entire Autobots team and Decepticons team are destroyed, there are no more battles and there are no survivors.

### How to run this app ###

- Run app in the Xcode simulator
- Tap "Select Fighters"
- There are preset list of transformers to be picked as fighters
- Tap each transformer that you would like to use
- You can also manually add your transformer by tapping "Add Transformer"
- Enter your transformer detail in the specified format
- There are basic validations on the text string entered to ensure the data is in correct format
- Once selection is complete, tap "Done"
- Tap "Begin Battle" to start the transformer battles