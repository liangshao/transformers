//
//  Transformer.swift
//  Transformers
//
//  Created by Liang Shao on 2018-02-17.
//  Copyright © 2018 Liang Shao. All rights reserved.
//

import UIKit

class Transformer: NSObject {
    
    var detail:String?
    
    var name:String?
    var team:String?
    
    var strength:Int?
    var intelligence:Int?
    var speed:Int?
    var endurance:Int?
    var rank:Int?
    var courage:Int?
    var firepower:Int?
    var skill:Int?
    
    var information: String?
    
    var selected:Bool = false
}
