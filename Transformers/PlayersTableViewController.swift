//
//  PlayersTableViewController.swift
//  Transformers
//
//  Created by Liang Shao on 2018-02-17.
//  Copyright © 2018 Liang Shao. All rights reserved.
//

import UIKit


class PlayersTableViewController: UITableViewController {
    
    var transformers = [Transformer]()

    @IBOutlet weak var doneButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        
        let plistPathInBundle = Bundle.main.path(forResource: "transformers", ofType: "plist")!
        let strings = NSArray(contentsOfFile:plistPathInBundle) as! [String]
        for string in strings {
            let t:Transformer = self.makeTransformerFromString(string)
            if t.name != nil {
                self.transformers.append(t)
            }
        }
        

        doneButton.isEnabled = false
        doneButton.tintColor = nil

        
    }
    
    @IBAction func doneButtonPressed(_ sender: UIBarButtonItem) {
        let selected = transformers.filter { $0.selected }
        AppManager.sharedInstance.transformers = selected
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func addTransformer(_ sender: Any) {
        let alert = UIAlertController(title: "Add Transformer", message: "Enter transformer detail in a text string like this: Soundwave, ​D, ​8,9,2,6,7,5,6,10", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.placeholder = "Soundwave, ​D, ​8,9,2,6,7,5,6,10"
        }
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let string = alert?.textFields![0].text
            //add to table view
            let t:Transformer = self.makeTransformerFromString(string!)
            t.selected = true
            if t.name != nil {
                self.transformers.append(t)
                self.tableView.reloadData()
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func makeTransformerFromString(_ string:String)->Transformer{
        let parts = string.components(separatedBy: ",")
        var t = Transformer()
        if(parts.count == 10){
            t.detail = string
            t.name = parts[0].trimmingCharacters(in: .whitespaces)
            t.team = parts[1].trimmingCharacters(in: .whitespaces)
            t.strength = Int(parts[2].trimmingCharacters(in: .whitespaces))
            t.intelligence = Int(parts[3].trimmingCharacters(in: .whitespaces))
            t.speed = Int(parts[4].trimmingCharacters(in: .whitespaces))
            t.endurance = Int(parts[5].trimmingCharacters(in: .whitespaces))
            t.rank = Int(parts[6].trimmingCharacters(in: .whitespaces))
            t.courage = Int(parts[7].trimmingCharacters(in: .whitespaces))
            t.firepower = Int(parts[8].trimmingCharacters(in: .whitespaces))
            t.skill = Int(parts[9].trimmingCharacters(in: .whitespaces))
            t.information = "[\(t.name!)] \n-------------\nStrength: \(t.strength ?? 0)\nIntelligence: \(t.intelligence ?? 0)\nSpeed: \(t.speed ?? 0)\nEndurance: \(t.endurance ?? 0)\nRank: \(t.rank ?? 0)\nCourage: \(t.courage ?? 0)\nFirepower: \(t.firepower ?? 0)\nSkill: \(t.skill ?? 0)\n\n"
            if((t.team != "A" && t.team != "D") || t.strength == nil || t.intelligence == nil || t.speed == nil || t.endurance == nil || t.rank == nil || t.courage == nil || t.firepower == nil || t.skill == nil){
                print("Error: invalid data")
                t = Transformer();
                let alert = UIAlertController(title: "Error", message: "Invalid data", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            print("Error: invalid data")
            let alert = UIAlertController(title: "Error", message: "Invalid data", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        return t
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return transformers.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...
        let transformer = transformers[indexPath.row]
        cell.textLabel?.text = transformer.detail
        
        if(transformer.selected){
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let transformer = transformers[indexPath.row]
        transformer.selected = !(transformer.selected)
        tableView.reloadData()
        let selected = transformers.filter { $0.selected }
        if selected.count > 1 {
            doneButton.isEnabled = true
            doneButton.tintColor = .red
        }else{
            doneButton.isEnabled = false
            doneButton.tintColor = nil
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
