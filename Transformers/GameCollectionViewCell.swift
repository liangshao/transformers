//
//  GameCollectionViewCell.swift
//  Transformers
//
//  Created by Liang Shao on 2018-02-18.
//  Copyright © 2018 Liang Shao. All rights reserved.
//

import UIKit

class GameCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var autobotTextView: UITextView!
    @IBOutlet weak var decepticonTextView: UITextView!
    @IBOutlet weak var roundLabel: UILabel!
}
