//
//  AppManager.swift
//  Transformers
//
//  Created by Liang Shao on 2018-02-17.
//  Copyright © 2018 Liang Shao. All rights reserved.
//

final class AppManager{
    static let sharedInstance = AppManager()
    var transformers = [Transformer]()
    private init(){
        
    }
}
