//
//  GameCollectionViewController.swift
//  Transformers
//
//  Created by Liang Shao on 2018-02-18.
//  Copyright © 2018 Liang Shao. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"
private let headerId = "Header"

var autobots = [Transformer]()
var decepticons = [Transformer]()
var autobotsWinningCount = 0
var decepticonsWinningCount = 0
var autobotSurvivors = [Transformer]()
var decepticonSurvivors = [Transformer]()

class GameCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var beginBattleButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        //let layout = self.collectionView?.collectionViewLayout;
        //layout
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
       // self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let transformers = AppManager.sharedInstance.transformers;
        autobots.removeAll()
        decepticons.removeAll()
        autobotsWinningCount = 0
        decepticonsWinningCount = 0
        autobotSurvivors.removeAll()
        decepticonSurvivors.removeAll()
        for transformer in transformers {
            let team = transformer.team
            if team == "A" {
                autobots.append(transformer)
            }else if team == "D" {
                decepticons.append(transformer)
            }
        }
        
        //sort by rank
        autobots = autobots.sorted(by: {$0.rank! > $1.rank!})
        decepticons = decepticons.sorted(by: {$0.rank! > $1.rank!})
        
        self.collectionView?.reloadData()
        
        if transformers.count > 1 {
            beginBattleButton.isEnabled = true
            beginBattleButton.tintColor = .red
        }else{
            beginBattleButton.isEnabled = false
            beginBattleButton.tintColor = nil
        }
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func beginBattle(_ sender: UIBarButtonItem) {
        let rounds = (autobots.count <= decepticons.count) ? autobots.count : decepticons.count
        var result = ""
        for i in 0..<rounds {
            print("Battle #\(i+1)")
            
            //In ​the ​event ​Optimus ​Prime ​and ​Predaking ​face ​each ​other, ​the ​game immediately ​ends ​with ​all ​competitors ​destroyed
            if autobots[i].name == "Optimus Prime" && decepticons[i].name == "Predaking"{
                let alert = UIAlertController(title: "Battle Result", message: "1 battle, All competitors destroyed", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            //Any ​Transformer ​named ​Optimus ​Prime ​or ​Predaking ​wins ​his ​fight ​automatically ​regardless ​of any ​other ​criteria
            if autobots[i].name == "Optimus Prime" {
                logWinningRecord(autobots[i])
                print("Winner is \(autobots[i].name!)")
                continue
            }
            if decepticons[i].name == "Predaking" {
                logWinningRecord(decepticons[i])
                print("Winner is \(decepticons[i].name!)")
                continue
            }
            
            //If ​any ​fighter ​is ​down ​4 ​or ​more ​points ​of ​courage ​and ​3 ​or ​more ​points ​of ​strength compared ​to ​their ​opponent, ​the ​opponent ​automatically ​wins ​the ​face-off ​regardless ​of overall ​rating ​(opponent ​has ​ran ​away)
            var winners:[Transformer] = compareCourageAndStrength(autobots[i], decepticons[i])
            
            print("Compared courage and strength")
            if winners.count == 1 {
                print("Winner is \(winners[0].name!)")
                logWinningRecord(winners[0])
            }else{
                
                //​if ​one ​of ​the ​fighters ​is ​3 ​or ​more ​points ​of ​skill ​above ​their ​opponent, ​they ​win the ​fight ​regardless ​of ​overall ​rating
                winners = compareSkill(autobots[i], decepticons[i])
                
                print("Compared skill")
                if winners.count == 1 {
                    print("Winner is \(winners[0].name!)")
                    logWinningRecord(winners[0])
                }else{
                    
                    //The ​winner ​is ​the ​Transformer ​with ​the ​highest ​overall ​rating
                    winners = compareOverallRating(autobots[i], decepticons[i])
                    print("Compared overall rating")
                    if winners.count == 1 {
                        print("Winner is \(winners[0].name!)")
                        logWinningRecord(winners[0])
                    }else{
                        result.append("It's a tie, both transformers destroyed")
                        continue
                    }
                }
            }
        }
        
        result.append("\(rounds) battle\n")
        
        if autobotsWinningCount == decepticonsWinningCount {
            result.append("It's a tie\n")
        }else{
            let winningTeam = autobotsWinningCount > decepticonsWinningCount ? "Autobots" : "Decepticons"
            
            let winners = autobotsWinningCount > decepticonsWinningCount ? autobotSurvivors : decepticonSurvivors
            let winnersNames = winners.map { $0.name! }
            result.append("Winning Team (\(winningTeam)) : \(winnersNames)\n")
            
            
            if winningTeam == "Autobots" {
                if (decepticons.count > rounds){
                    decepticons.removeSubrange(0..<rounds)
                    for decepticon in decepticons {
                        decepticonSurvivors.append(decepticon)
                    }
                }
                let nameArray = decepticonSurvivors.map { $0.name! }
                if nameArray.count == 0 {
                    result.append("No survivors of the losing team (Decepticons)")
                }else{
                    result.append("Survivors of the losing team (Decepticons): \(nameArray)")
                }
            }else if winningTeam == "Decepticons"{
                if (autobots.count > rounds){
                    autobots.removeSubrange(0..<rounds)
                    for autobot in autobots {
                        autobotSurvivors.append(autobot)
                    }
                }
                let nameArray = autobotSurvivors.map { $0.name! }
                if nameArray.count == 0 {
                    result.append("No survivors of the losing team (Autobots)")
                }else{
                    result.append("Survivors of the losing team (Autobots): \(nameArray)")
                }
            }
            
            
        }
        
        let alert = UIAlertController(title: "Battle Result", message: result, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func compareCourageAndStrength(_ t1:Transformer, _ t2:Transformer) -> [Transformer]{
        var winners = [t1, t2]
        if (t1.courage! >= t2.courage! + 4) && (t1.strength! >= t2.strength! + 3) {
            winners.removeLast()
        }else if (t2.courage! >= t1.courage! + 4) && (t2.strength! >= t1.strength! + 3) {
            winners.removeFirst()
        }
        return winners
    }
    
    func compareSkill(_ t1:Transformer, _ t2:Transformer) -> [Transformer]{
        var winners = [t1, t2]
        if t1.skill! >= t2.skill! + 3 {
            winners.removeLast()
        }else if  t2.skill! >= t1.skill! + 3 {
            winners.removeFirst()
        }
        return winners
    }
    
    func compareOverallRating(_ t1:Transformer, _ t2:Transformer) -> [Transformer]{
        var winners = [t1, t2]
        if t1.strength! + t1.intelligence! + t1.speed! + t1.endurance! + t1.firepower! > t2.strength! + t2.intelligence! + t2.speed! + t2.endurance! + t2.firepower!{
            winners.removeLast()
        }else if t1.strength! + t1.intelligence! + t1.speed! + t1.endurance! + t1.firepower! < t2.strength! + t2.intelligence! + t2.speed! + t2.endurance! + t2.firepower! {
            winners.removeFirst()
        }
        return winners
    }
    
    func logWinningRecord(_ t:Transformer){
        if t.team == "A" {
            autobotsWinningCount += 1
            autobotSurvivors.append(t)
        }else if t.team == "D" {
            decepticonsWinningCount += 1
            decepticonSurvivors.append(t)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return (autobots.count >= decepticons.count) ? autobots.count : decepticons.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        // Configure the cell
        let cell:GameCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! GameCollectionViewCell
        let index = indexPath.row
        let smallest = min(autobots.count, decepticons.count)
        if smallest > index {
            cell.roundLabel.text = "Battle #\(index+1)"
        }else{
            cell.roundLabel.text = "No battle"
        }
        if (index < autobots.count){
            let transformer = autobots[index]
            cell.autobotTextView.text = transformer.information
        }else{
            cell.autobotTextView.text = nil
        }
        if (index < decepticons.count){
            let transformer = decepticons[index]
            cell.decepticonTextView.text = transformer.information
        }else{
            cell.decepticonTextView.text = nil
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath as IndexPath)
        return headerView
    }


    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
